CREATE database hospital;
use hospital;

create table IF NOT EXISTS operaciones(
id int primary key auto_increment,
sala varchar(50) not null unique,
fecha date,
duracion int DEFAULT 0,
id_paciente INT UNSIGNED NOT NULL REFERENCES pacientes

);

create table IF NOT EXISTS pacientes(
id int primary key auto_increment,
nombre varchar(50) not null unique,
apellidos varchar(50),
edad int DEFAULT 0,
fecha_nac date
);

create table IF NOT EXISTS doctores(
id int primary key auto_increment,
nombre varchar(50) not null unique,
apellidos varchar(50),
num_expediente int DEFAULT 0,
fecha_nac date
);

create table IF NOT EXISTS especialidades(
id int primary key auto_increment,
nombre varchar(50) not null unique,
anios int DEFAULT 0,
fecha_titulacion date
);

CREATE TABLE IF NOT EXISTS pacientes_operaciones(
	id_paciente INT UNSIGNED REFERENCES pacientes,
	id_operacion INT UNSIGNED REFERENCES operaciones,
	PRIMARY KEY (id_paciente, id_operacion)
);
CREATE TABLE IF NOT EXISTS especialidades_doctores(
	id_especialidad INT UNSIGNED REFERENCES especialidades,
	id_doctor INT UNSIGNED REFERENCES doctores,
	PRIMARY KEY (id_especialidad, id_doctor)
);
CREATE TABLE IF NOT EXISTS doctores_pacientes(
	id_doctor INT UNSIGNED REFERENCES doctores,
	id_paciente INT UNSIGNED REFERENCES pacientes,
    diagnostico varchar(50),
	fecha date,
	PRIMARY KEY (id_doctor, id_paciente)
);